import pandas as pd
import streamlit as st
st.title('Exploratory Dashboard') # import data
# st.markdown("Lets look at the _Google_ dataset from Kaggle: [Link] (https://www.kaggle.com/lava18/google-play-store-apps#googleplaystore.csv)")
def load_data():
  data = pd.read_csv('./data/googleplaystore.csv')
  return data
data = load_data()
subset = data[data.Category != '1.9']
if st.checkbox('Show Data'):
  subset
st.markdown("_Lets explore the data using scatter plot..._")
#chart parameters
x_axis = st.sidebar.selectbox('Select X-axis:', ('Rating','Reviews', 'Size', 'Installs'))
y_axis = st.sidebar.selectbox('Select Y-axis:', ('Reviews', 'Rating','Size', 'Installs'))
size = st.sidebar.selectbox('Select Size Metric:', ('Installs', 'Rating','Size', 'Reviews'))
color = st.sidebar.selectbox('Select Color Metric:', ('Content Rating','Type', 'Genres','Installs'))
#dashboard filters
Catergory = st.sidebar.multiselect("Category", subset['Category'].unique())
Type = st.sidebar.multiselect("Type", subset['Type'].unique())
Genres = st.sidebar.multiselect("Genres", subset['Genres'].unique())
if Catergory:
  subset = subset[subset.Category.isin(Catergory)]
if Type:
  subset = subset[subset.Type.isin(Type)]
if Genres:
  subset = subset[subset.Genres.isin(Genres)]
# build chart
st.vega_lite_chart(subset, {
  'width': 'container',
  'height': 400,
  'mark':'circle',
  'encoding':{
    'x':{
      'field':'Rating',
      'type': 'quantitative'
        },
    'y':{
      'field':'Reviews',
      'type':'quantitative'
        },
    'size':{ 
      'field':'Rating',
      'type':'quantitative'},
    'color':{
      'field':'Content Rating',
      'type':'nominal'}
  }
}, use_container_width=True)
